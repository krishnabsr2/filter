import { Component, OnInit } from '@angular/core';
import { Car } from '../shared/car';
 

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  id : string;
  portalid: string[];
  cars: Car[] = [
    {
      CAR_MODEL : 'XZ(A)',
      SEL_COLOR: 'Berry red',
      image: '/assets/images/car.jpg',
      amount : 45000,
      port_ID : 'Txcvbnm123'
      
    },
    {
      CAR_MODEL : 'XZ(B)',
      SEL_COLOR: 'Violet',
      image: '/assets/images/car.jpg',
      amount : 52000,
      port_ID : 'Txcvbnm221'
    },
    {
      CAR_MODEL : 'XZ(C)',
      SEL_COLOR: 'METAL BLUE',
      image: '/assets/images/car.jpg',
      amount : 48000,
      port_ID : 'Txcvbnm221'
    },
    {
      CAR_MODEL : 'XZ(D)',
      SEL_COLOR: 'WHITE',
      image: '/assets/images/car.jpg',
      amount : 56000,
      port_ID : 'Txcvbnm221'
    },
    {
      CAR_MODEL : 'XZ(E)',
      SEL_COLOR: 'Thunder Black',
      image: '/assets/images/car.jpg',
      amount : 75000,
      port_ID : 'Txcvbnm224'
    }
  ] 
  constructor() { 
    this.id = '';
    this.portalid = [
      '1aa2345df',
      '1aa2345fg',
      '1aa2345zx',
      '1aa2345xc',
      '1aa2345cv',
      '1aa2345vb'
    ]
  }

  

  

  ngOnInit() {
  }


  d(id:string){
    console.log(id);
  }
}

    


